//
//  ViewController.swift
//  GetAcc
//
//  Created by Stewart Boyd on 7/11/16.
//  Copyright © 2016 Stewart Boyd. All rights reserved.
//

import CoreMotion
import UIKit
import MessageUI

import UIKit

extension String {
    func appendLineToURL(fileURL: URL) throws {
        try self.appending("\n").appendToURL(fileURL: fileURL as NSURL)
    }
    
    func appendToURL(fileURL: NSURL) throws {
        let data = self.data(using: .utf8)

    }
}

extension Data {
    func appendToURL(fileURL: URL) throws {
        if let fileHandle = try? FileHandle(forWritingTo: fileURL) {
            defer {
                fileHandle.closeFile()
            }
            fileHandle.seekToEndOfFile()
            fileHandle.write(self)
        }
        else {
            try self.write(to: fileURL, options: .atomic)
        }
    }
}

class ViewController: UIViewController, MFMailComposeViewControllerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var fileNameTextField: UITextField!
    @IBOutlet weak var toggleRecordingButton: UIBarButtonItem!
    @IBOutlet weak var sendRecordingButton: UIBarButtonItem!
    @IBOutlet weak var toolbar: UIToolbar!


    let mailComposer = MFMailComposeViewController()
    lazy var textToWrite = [NSIndexPath:Operation]()
    var queue = OperationQueue()
    let fileManager = FileManager.default
    
    
    let MyMotionManager = CMMotionManager()
    var recordData = true
    var buttonRecord = true
    var fileName = "accelerometerData.csv"
    var fileURL : URL?
    let accelerometerUpdateInterval = 0.01
    var start : Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MyMotionManager.accelerometerUpdateInterval = accelerometerUpdateInterval
        determineFileName()
        fileNameTextField.delegate = self
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func determineFileName(){
        //only set filename if text field filled out
        if let text = fileNameTextField.text{
            //as long as the text field isn't blank proceed to update it
            if text != "" {
                fileName = "\(text).csv"
            }
        }
        if let dir = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.allDomainsMask, true).first {
            fileURL = NSURL(fileURLWithPath: dir).appendingPathComponent(fileName)
        }
        else{
            print("filePath not constructured")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func toggleRecordingButtonTouched(_ sender: Any) {
        print("toggleRecordingButtonTouched")
        if buttonRecord{
            startRecording()
        }
        else{
            stopRecording()
        }
        buttonRecord = !buttonRecord
        print("buttonFinished Click")
    }

    @IBAction func sendRecordingButtonTouched(_ sender: Any) {
        sendEmail()
    }

    func sendEmail() {
        //Check to see the device can send email.
        if( MFMailComposeViewController.canSendMail() ) {
            print("Can send email.")
            
            
            mailComposer.mailComposeDelegate = self
            
            //Set the subject and message of the email
            mailComposer.setSubject("Have you heard a swift?")
            mailComposer.setMessageBody("This is what they sound like.", isHTML: false)
            
            if let url = fileURL {
                
                print("File path loaded.")
                print("\(url)")
                
                if fileManager.fileExists(atPath: url.path) {
                    print("FILE AVAILABLE")
                } else {
                    print("FILE NOT AVAILABLE")
                }
                do {
                    let fileData = try Data(contentsOf: url)
                    mailComposer.addAttachmentData(fileData, mimeType: "text/csv", fileName: fileName)
                } catch {
                    print("Couldn't compose mail")
                }
            }
            self.present(mailComposer, animated: true, completion: nil)
        }
    }
    
    func deleteFilePath(fpath : String){
        do {
            try fileManager.removeItem(atPath: fpath)
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
    }
    
    func mailComposeController(controller: MFMailComposeViewController,
                               didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        // Check the result or perform other tasks.
        
        // Dismiss the mail compose view controller.
        if let e = error {
            print("error")
            print(e)
        }
        else{
            print("NO Error")
        }
        controller.dismiss(animated: true, completion: nil)
    }

    func writeAccData(accData : CMAccelerometerData?, e : Error?) -> Void{
        let time = NSDate()
        let interval = time.timeIntervalSince(start!)
        let ad = MyMotionManager.accelerometerData!
        let text = "\(interval), \(ad.acceleration.x), \(ad.acceleration.y),\(ad.acceleration.z)\n"
        print(text)
        writeText(textToWrite: text)
    }
    
    func writeText(textToWrite : String){
        print("writeText")
        if let url = fileURL{
            let fileManager = FileManager.default
            print(url.path)
            print(url.absoluteString)
            if fileManager.fileExists(atPath: url.path){
                print("FILE AVAILABLE")
                try! textToWrite.appendToURL(fileURL: url as NSURL)
            } else {
                print("FILE NOT AVAILABLE")
                try! textToWrite.write(to: url, atomically: false, encoding: .utf8)
            }
            
            if fileManager.fileExists(atPath: url.path) {
                print("FILE AVAILABLE")
            } else {
                print("FILE NOT AVAILABLE")
            }
        }
        else{
            print("Error with filepath")
        }
        print("finishWriteText")
    }
    
    func startRecording(){
        print("startRecording")
        toggleRecordingButton.image = UIImage.init(imageLiteralResourceName: "pause")
        determineFileName()
        start = Date()
        writeText(textToWrite: "Time, Acc X, Acc Y, Acc Z\n")
        if MyMotionManager.isAccelerometerAvailable {
            MyMotionManager.startAccelerometerUpdates(to: queue, withHandler: self.writeAccData)
        }
    }

    func stopRecording(){
        print("stopRecording")
        toggleRecordingButton.image = UIImage.init(imageLiteralResourceName: "play")
        recordData = false;
        if MyMotionManager.isAccelerometerAvailable {
            MyMotionManager.stopAccelerometerUpdates()
        }
    }
    
}
